#[allow(deprecated)]
use teloxide::{prelude::*, utils::command::BotCommand, types::{ParseMode::{Html, Markdown}, InputFile, InlineKeyboardButton, InlineKeyboardMarkup, InlineKeyboardButtonKind, ReplyMarkup}};
use std::process::{Command, Stdio};
use std::process::Command as Exec;
use std::process;
use std::io::{Read, Write};
use std::error::Error;
use std::time::Instant;
use std::time;
use std::env;
use std::thread::sleep;
use std::path::{Path, PathBuf};
use ipinfo::{IpInfo, IpInfoConfig};
use rand::Rng;
use serde_json::Value;
use reqwest;

#[derive(BotCommand)]
#[command(rename = "lowercase", description = "These commands are supported:")]
enum Tgcmd {
    #[command(description = "display this text.")]
    Help,
    
    #[command(description = "Prints random integer('lucky number xDD')")]
    L,
    
    #[command(prefix = "k.", description = "Downloads a document(Only for kNIGHT).", parse_with = "split")]
    Dl { dllink: String, output: String },

    #[command(description = "Prints current running processes.")]
    Ps,

    #[command(prefix = "k.", description = "Executes command in sub-shell(Only for kNIGHT).")]
    Sh { cmd: String },

    #[command(prefix = "k.", description = "Uploads a document(Only for kNIGHT).")]
    Ul { file: String },

    #[command(description = "Sends a cat pic")]
    Cat { kat: i64 },

    #[command(description = "Sends text.")]
    Msg { text: String },
    
    #[command(description = "Shows neofetch output.")]
    Neo,

    #[command(description = "Gets IP info.")]
    Ipa { addr: String },
    
    #[command(description = "Shows what is the command about.")]
    Man { manpage: String },

    #[command(description = "Runnns ^o^.")]
    Run,

    #[command(description = "Gets UserID & ChatID.")]
    Uid,

    #[command(description = "Gets definition of word from urban dictionary.")]
    Urb { ud: String },

    #[command(description = "Gets the last redirected URL.")]
    Link { lonk: String },

    #[command(description = "Checks alive/dead.")]
    Ping,

    #[command(description = "Sends a rtfm message")]
    Rtfm,

    #[command(prefix = "k.", description = "Shuts down the bot(Only for kNIGHT).", parse_with = "split" )]
    Shut,

    #[command(description = "Sends source code of bot.")]
    Sauce,

    #[command(description = "Starts the bot(For PM).")]
    Start,

    #[command(description = "Gets WHOIS info of sites.")]
    Whois { who: String },

    #[command(description = "Sends a why do you ask text.")]
    Anyone,
}

async fn get_def(taxt: &String) -> Option<String> {
    let url = format!("https://api.urbandictionary.com/v0/define?term={}",taxt);
    let response = match make_request(url).await {
        Some(val) => val,
        None => return None,
    };
    if response["list"].as_array().unwrap().is_empty() {
        return Some(String::from("No definition found!"));
    }
    let target = &response["list"][0]["definition"];
    Some(target.to_string().trim_matches('"').to_string())

}    

async fn make_request(url: String) -> Option<Value> {

    let response = reqwest::get(&url).await.ok()?.text().await.ok();

    let response = match response {
        Some(val) => val.trim().to_string(),
        None => return None,
    };
    let response: Value = match serde_json::from_str(&response) {
        Ok(val) => val,
        _ => return None,
    };

    Some(response)
}
#[allow(deprecated)]
async fn answer(
    cx: UpdateWithCx<AutoSend<Bot>, Message>,
    command: Tgcmd,
) -> Result<(), Box<dyn Error + Send + Sync>> {
    match command {
       Tgcmd::Help =>  cx.reply_to(format!("Hello There!, 
I am a bot made by [YOURNAME](https://t.me/YOURUSERNAME) in [rust](https://www.rust-lang.org) based on [teloxide](https://github.com/teloxide/teloxide)
Here's a list of my commands:-
`/help` ~ _Display this text._
`/l` ~ _Prints random integer('lucky number xDD')._
`/ps` ~ _Prints current running processes._
`/cat [http code] ` ~ _Sends cat pic according to http codes_
`/msg [text to say]` ~ _Sends text._
`/neo` ~ _Shows neofetch output._
`/ipa [ip]` ~ _Gets info of given IP._
`/man [command]` ~ _Shows what is the command about._
`/run` ~ _Runnns ^o^._
`/uid` ~ _Gets UserID & ChatID._
`/urb [word]` ~ _Gets definition of word from urban dictionary._
`/link [url]` ~ _Gets the last redirected URL._
`/ping` ~ _Checks Alive/Dead._
`/rtfm` ~ _Sends a RTFM message._
`/sauce` ~ _Sends source code of bot._
`/whois [url]` ~ _Gets WHOIS info of site._
`/anyone` ~ _Sends a why do you ask text._")).parse_mode(Markdown).disable_web_page_preview(true).await?,
 
	Tgcmd::Msg { text } => {
	    let output: i32 = if let Some(reply) = cx.update.reply_to_message() {
		reply.id
	    } else {
		cx.update.id
	    };
	    if text.trim().is_empty() {
		cx.answer(format!("Send what? Give me <b>any text</b> to send!")).parse_mode(Html).reply_to_message_id(output).await?
	    } else {
		cx.answer(text).reply_to_message_id(output).parse_mode(Markdown).await?
	    }
	}
    
	Tgcmd::Neo => {
	    let ne = Command::new("osfetch-rs").arg("--stdout").output().expect("failed to execute command");
	    cx.answer(format!("`{}`", String::from_utf8_lossy(&ne.stdout))).parse_mode(Markdown).await?
	}
    
	Tgcmd::Ipa { addr } => {
	    if addr.trim().is_empty() {
		cx.reply_to(format!("Send a <b>IP address</b>!")).parse_mode(Html).await?
	    } else {
		let msg = cx.reply_to(format!("<b>Extracting info from ip addr.....</b>")).parse_mode(Html).await?;
		sleep(time::Duration::new(2,0));
		let config = IpInfoConfig { token: Some("dabd4b146a4ba7".to_string()), ..Default::default() };
		let mut ipinfo = IpInfo::new(config).expect("should construct :P");
		let res = ipinfo.lookup(&[&addr]);
		match res {
		    Ok(ref r) => println!("{}: {:#?}", "8.8.8.8", r[&addr].hostname),
		    Err(ref e) => println!("error occurred: {}", &e.to_string()),
		}
		cx.requester.edit_message_text(msg.chat_id(), msg.id, format!("*{:#?}*", res)).parse_mode(Markdown).await?
	    }
	}
  
	Tgcmd::Man { manpage } => {
	    if manpage.trim().is_empty() {
		cx.reply_to(format!("Send which <b>command</b> you want to know about!")).parse_mode(Html).await?
	    } else {
		let mn = Command::new("man").arg("-f").arg(manpage).output().expect("failed to execute command");
		cx.reply_to(format!("`{}`", String::from_utf8_lossy(&mn.stdout))).parse_mode(Markdown).await?
	    }
	}

	Tgcmd::Link { lonk } => {
	    if lonk.trim().is_empty() {
		cx.reply_to(format!("Send a <b>proper URL</b>!")).parse_mode(Html).await?
	    } else {
		let msg = cx.reply_to(format!("<b>Extracting redirected URL from given link.....</b>")).parse_mode(Html).await?;
		sleep(time::Duration::new(2,0));
		let mut cmd_crl = Exec::new("curl")
                    .arg("-sLI")
                    .arg(lonk)
                    .stdout(Stdio::piped())
	            .spawn()
                    .unwrap();
		
		let mut cmd_grep = Exec::new("grep")
                    .arg("-i")
                    .arg("Location")
                    .stdin(Stdio::piped())
                    .stdout(Stdio::piped())
                    .spawn()
                    .unwrap();
		if let Some(ref mut stdout) = cmd_crl.stdout {
                    if let Some(ref mut stdin) = cmd_grep.stdin {
			let mut buf: Vec<u8> = Vec::new();
			stdout.read_to_end(&mut buf).unwrap();
			stdin.write_all(&buf).unwrap();
		    }
		}

		let res = cmd_grep.wait_with_output().unwrap().stdout;
		cx.requester.edit_message_text(msg.chat_id(), msg.id, format!("`{:#?}`", String::from_utf8_lossy(&res))).parse_mode(Markdown).await?
            }
	}

	Tgcmd::Urb { ud } => {
	    if ud.trim().is_empty() {
		let msg = cx.reply_to(format!("<b>Getting definition of random word from urban dictionary.....</b>")).parse_mode(Html).await?;
		let url = "http://api.urbandictionary.com/v0/random";
		let response = make_request(url.to_string()).await;
		let word = &response.clone().unwrap()["list"][0]["word"];
		let defin = &response.clone().unwrap()["list"][0]["definition"];
		cx.requester.edit_message_text(msg.chat_id(), msg.id, format!("Definition for <b>{}</b> : <i>{}</i>", word.to_string().trim_matches('"').to_string(), defin.to_string().trim_matches('"').to_string().replace("\r\n", ""))).parse_mode(Html).await?
	    } else {
		let msg = cx.reply_to(format!("<b>Getting definition of word from urban dictionary.....</b>")).parse_mode(Html).await?;
		let defin = get_def(&ud).await;
		if defin.is_none() {
		    cx.requester.edit_message_text(msg.chat_id(), msg.id, "something went wrong").await?
		} else {
		    cx.requester.edit_message_text(msg.chat_id(), msg.id, format!("Definition for <b>{}</b> : <i>{}</i>", ud, defin.unwrap().replace("\r\n", ""))).parse_mode(Html).await?
		}
	    }
	}

	Tgcmd::Whois { who } => {
	    if who.trim().is_empty() {
		cx.reply_to(format!("Send a <b>proper URL</b> to get WHOIS info!")).parse_mode(Html).await?
	    } else {
		let msg = cx.reply_to(format!("<b>Extracting WHOIS info from given link.....</b>")).parse_mode(Html).await?;
		sleep(time::Duration::new(2,0));
		let mut wh = Exec::new("whois")
		    .arg(who)
		    .stdout(Stdio::piped())
		    .spawn()
		    .unwrap();
		let mut wh_grep = Exec::new("rg")
		    .arg("Registrar")
		    .stdin(Stdio::piped())
		    .stdout(Stdio::piped())
		    .spawn()
		    .unwrap();
		if let Some(ref mut stdout) = wh.stdout {
		    if let Some(ref mut stdin) = wh_grep.stdin {
			let mut buf: Vec<u8> = Vec::new();
			stdout.read_to_end(&mut buf).unwrap();
			stdin.write_all(&buf).unwrap();
		    }
		}
		let res = wh_grep.wait_with_output().unwrap().stdout;
		cx.requester.edit_message_text(msg.chat_id(), msg.id, format!("`{}`", String::from_utf8_lossy(&res))).parse_mode(Markdown).await?
	    }
	}

	Tgcmd::Ps => {
	    let p = Command::new("ps").output().expect("failed to execute command");
	    cx.reply_to(format!("<code>{}</code>", String::from_utf8_lossy(&p.stdout))).parse_mode(Html).await?
	}

	Tgcmd::Sh { cmd } => {
		if cx.update.from().unwrap().id == ID_HERE {
		    if cmd.trim().is_empty() {
			cx.reply_to(format!("Dude! with all due respect that you're my maker and all, give me a <b>proper command</b> to run!")).parse_mode(Html).await?
		    } else {
			let shout  = Exec::new("bash").arg("-c").arg(cmd).output().expect("FAIL");
			cx.reply_to(format!("`{}`\n*{}*\n{}", String::from_utf8_lossy(&shout.stdout), shout.status, String::from_utf8_lossy(&shout.stderr))).parse_mode(Markdown).await?
		    }
		} else {
		cx.reply_to(format!("<b>Works for kNIGHT only!</b>")).parse_mode(Html).await?
		}
            }

	Tgcmd::Run => {
	    let start = Instant::now();
	    let elapsed = start.elapsed();
	    let el = elapsed.subsec_nanos() % 3;
	    let a = String::from("The winter dog is running......");
	    let b = String::from("Run away and never come back......");
	    let c = String::from("Let's keep running folks!");
	    if el == 0 {
		cx.reply_to(format!("<b>{}</b>", a)).parse_mode(Html).await?
	    } else if el == 1 {
		cx.reply_to(format!("<b>{}</b>", b)).parse_mode(Html).await?
	    } else {
		cx.reply_to(format!("<b>{}</b>", c)).parse_mode(Html).await?
	    }
	}

	Tgcmd::L => {
	    let lu: u8 = rand::thread_rng()
		.gen_range(1, 100);
	    cx.reply_to(format!("Your lucky number is: <code>{}</code>", lu)).parse_mode(Html).await?
	}

	Tgcmd::Shut => {
	    if cx.update.from().unwrap().id == ID_HERE {
		cx.reply_to(format!("bai")).await?;
		    process::exit(0);
	    } else {
		cx.reply_to(format!("<code>Shut yourself before trying to shut me down!</code>")).parse_mode(Html).await?
		}
	}

	Tgcmd::Dl { dllink, output } => {
	    if cx.update.from().unwrap().id == ID_HERE {
		let msg = cx.reply_to("<b>Downloading File...!</b>").parse_mode(Html).await?;
		Exec::new("wget").arg(dllink).arg("-O").arg(&output).output().expect("failed to execute command");
		cx.requester.edit_message_text(msg.chat_id(), msg.id, format!("<b>File <code>{}</code> has been downloaded!</b>", output)).parse_mode(Html).await?
	    } else {
		cx.reply_to(format!("nou")).await?
	    }
	}

	Tgcmd::Ul { file } => {
	    if cx.update.from().unwrap().id == ID_HERE {
		if Path::new(&file).exists() {
		    let send = InputFile::file(PathBuf::from(file));
		    cx.requester.send_document(cx.update.chat_id(), send).reply_to_message_id(cx.update.id).await?
		} else {
		    cx.reply_to("File not found!").await?
		}
	    } else {
		cx.reply_to(format!("nou")).await?
	    }
	}

	Tgcmd::Cat { kat } => cx.answer_photo(InputFile::Url(format!("https://http.cat/{}.jpg", kat).to_string())).await?,
	
	Tgcmd::Rtfm => {
	    let output = if let Some(reply) = cx.update.reply_to_message() {
		reply.id
	    } else {
		cx.update.id
	    };
	    cx.answer(format!("How bout you...")).reply_markup(ReplyMarkup::InlineKeyboard(InlineKeyboardMarkup::new(vec![vec![InlineKeyboardButton::new("Read the fucking manual!", InlineKeyboardButtonKind::Url("http://readthefuckingmanual.com".into()))]]))).reply_to_message_id(output).await?
	}
	
	Tgcmd::Anyone => {
	    let output = if let Some(reply) = cx.update.reply_to_message() {
		reply.id
	    } else {
		cx.update.id
	    };
	    cx.answer(format!("Hmm.")).reply_markup(ReplyMarkup::InlineKeyboard(InlineKeyboardMarkup::new(vec![vec![InlineKeyboardButton::new("Why do you ask?", InlineKeyboardButtonKind::Url("https://dontasktoask.com".into()))]]))).reply_to_message_id(output).await?
	}
	
	Tgcmd::Sauce => {
	    let output = if let Some(reply) = cx.update.reply_to_message() {
		reply.id
	    } else {
		cx.update.id
	    };
	    cx.answer(format!("You asked for it, so here you go!")).reply_markup(ReplyMarkup::InlineKeyboard(InlineKeyboardMarkup::new(vec![vec![InlineKeyboardButton::new("sauce", InlineKeyboardButtonKind::Url("https://gitlab.com/cyberknight777/knight-bot".into()))]]))).reply_to_message_id(output).await?
	}

	Tgcmd::Ping => {	    
	    let start = time::Instant::now();
	    let message = cx.reply_to(format!("<b>Ping...</b>")).parse_mode(Html).await?;
	    sleep(time::Duration::new(1,0));
	    let end = time::Instant::now();
	    let differ = end.duration_since(start).as_millis();
	    let duration = differ / 10;
	    cx.requester.edit_message_text(message.chat_id(), message.id, format!("<b>Pong!</b> ~> <i>{}ms</i>", duration)).parse_mode(Html).await?
	}
	
	Tgcmd::Uid => {
	        if let Some(reply) = cx.update.reply_to_message() {
        cx.reply_to(format!("ID: <code>{}</code>\nChatID: <code>{}</code>\n{}'s ID: <code>{}</code>", cx.update.from().unwrap().id, cx.update.chat_id(), reply.from().unwrap().first_name, reply.from().unwrap().id)).parse_mode(Html).await?
    } else {
        cx.reply_to(format!("ID: <code>{}</code>\nChatID: <code>{}</code>", cx.update.from().unwrap().id, cx.update.chat_id())).parse_mode(Html).await?
    }
	}
	
	Tgcmd::Start => cx.reply_to(format!("Heya! Type /help to see what i can do!!")).await?,
    	  
    };

    Ok(())
	
}

#[tokio::main]
async fn main() {
    teloxide::enable_logging!();
    log::info!("Starting BOT-NAME...");

    let bot = Bot::from_env().auto_send();

    let bot_name: String = "BOT_USERNAME".to_owned();
    teloxide::commands_repl(bot, bot_name, answer).await;
}
